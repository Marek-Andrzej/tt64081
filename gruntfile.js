const sass = require("node-sass");
const util = require("util");
const fs = require("fs");
require("util.promisify").shim();

var project = "tefal";
var prefix = "TFL_";
var title = "TEFAL";

var dirs = ["TT64081"];

var packages = {
  TT64081: ["RtvEuroAgd"]
};

var buildConfig = [
  {
    name: "MediaMarkt",
    flatten: false,
    //inputFile: "output.html",
    video: "/assets/video1.mp4",
    fullHtml: false,
    usePartials: false
  },
  {
    name: "MediaExpert",
    flatten: true,
    video: "/assets/video1.mp4",
    //inputFile: "output.html",
    fullHtml: false,
    usePartials: false
  },
  {
    name: "RtvEuroAgd",
    flatten: false,
    //inputFile: "rtvoutput.html",
    video: "/assets/video1.mp4",
    usePartials: false
  },
  {
    name: "NeoNet",
    flatten: false,
    //inputFile: "output.html",
    video: "/assets/video1.mp4",
    fullHtml: true,
    usePartials: false
  }
];

var video = "/test/video.mp4";

var availablePartials = [];

module.exports = function(grunt) {
  var mode = grunt.option("mode") || "preview";
  var basedir = "variations/" + dirs[0] + "/";

  var wathcHtmlForChange = [
    //'index.html', 'html_parts/*.html'
  ];

  var concatObj = {
    main: {
      src: [
        "html_parts/header.html",
        basedir + "index.html",
        "html_parts/video.html",
        "html_parts/footer.html"
      ],
      dest: basedir + "output.html"
    }
  };

  // var concatObj;

  var flatFiles = {};
  var replacePatterns = {};

  var pngminFilesArray = [
    {
      expand: true,
      cwd: "assets/dev/",
      src: ["**/*.png"],
      dest: "assets/"
    }
  ];

  for (var k in dirs) {
    console.log(dirs[k]);
    pngminFilesArray.push({
      expand: true,
      cwd: "variations/" + dirs[k] + "/assets/dev/",
      src: ["**/*.png"],
      dest: "variations/" + dirs[k] + "/assets/"
    });

    concatObj[dirs[k]] = {
      src: [
        "html_parts/header.html",
        "variations/" + dirs[k] + "/index.html",
        "html_parts/video.html",
        "html_parts/footer.html"
      ],
      dest: "variations/" + dirs[k] + "/output.html"
    };
    wathcHtmlForChange.push("variations/" + dirs[k] + "/index.html");

    for (var a in availablePartials) {
      var repFile =
        "html_parts/variations/" +
        availablePartials[a] +
        "/" +
        dirs[k] +
        "/base.html";
      replacePatterns[dirs[k] + availablePartials[a]] = {
        options: {
          patterns: [
            {
              match: "include_" + availablePartials[a],
              replacement: '<%= grunt.file.read("' + repFile + '") %>'
            }
          ]
        },
        files: [
          {
            //expand: true, flatten: true,
            src: ["variations/" + dirs[k] + "/output.html"],
            dest: "variations/" + dirs[k] + "/output.html"
          }
        ]
      };
      wathcHtmlForChange.push(repFile);
    }
  }
  var pngminObj = {
    assets: {
      options: {
        concurrency: 8,
        ext: ".png",
        quality: "65-80",
        force: true
      },
      files: pngminFilesArray
    }
  };
  var inlineConfig = {
    dist: {
      options: {
        cssmin: true,
        uglify: true
      },
      src: basedir + "/output.html",
      dest: "../release/preview/index.html"
    }
  };
  var cleanArr = ["../release/preview.zip"];
  var copyObj = {
    main: {
      files: [
        {
          expand: true,
          src: [basedir + "assets/*"],
          dest: "../release/preview/",
          filter: "isFile"
        }
      ]
    }
  };
  var compressObj = {
    main: {
      options: {
        archive: "../release/preview.zip"
      },
      expand: true,
      cwd: "../release/preview/",
      src: ["**/*"],
      dest: "../"
    }
  };

  var dom = {
    update: {
      options: {
        update: {
          selector: "#video",
          attribute: "src",
          value: video
        },
        callback: function($, file) {
          let classExist = $("img").hasClass("lazy");
          let attribute = $("img").attr("data-original");

          if (!classExist) {
            $("img").addClass("lazy");
          }
          if (!attribute) {
            $("img").each(function() {
              $(this)
                .attr("data-original", $(this).attr("src"))
                .attr("src", "f00.esfr.pl/images/www/blank.gif");
            });
          }
        }
      },
      src: "./variations/TT64081/index.html",
      dest: "test/" + dir + "/"
    }
  };

  if (mode != "preview") {
    inlineConfig = {};
    cleanArr = [];
    copyObj = {};
    compressObj = {};
    dom = {};
    concatObj = {};
    replacePatterns = {};

    var inputFile = "index.html";
    var releaseBase = "../release/variations/";
    var partsVariantions = "html_parts/variations/";

    for (var k in buildConfig) {
      for (var j in dirs) {
        var dir = dirs[j];
        var check_video = buildConfig[k].video || video;
        var tmp = [];
        tmp.push("variations/" + dir + "/index.html");

        if (buildConfig[k].fullHtml) {
          tmp.unshift("html_parts/header.html");
          tmp.push("html_parts/footer.html");
        }

        concatObj[k + dir] = {
          src: tmp,
          dest: "variations/" + dir + "/" + buildConfig[k].name + ".html"
        };

        if (packages[dir].indexOf(buildConfig[k].name) != -1) {
          var releaseDirName =
            releaseBase + dir + "_" + buildConfig[k].name + "/";
          var inputDirName = "./variations/" + dir + "/";

          if (typeof buildConfig[k].inputFile != "undefined") {
            if (Array.isArray(buildConfig[k].inputFile)) {
              for (var f in buildConfig[k].inputFile) {
                if (
                  grunt.file.exists(inputDirName + buildConfig[k].inputFile[f])
                ) {
                  //console.log('exists', inputDirName + buildConfig[k].inputFile[f])
                  inputFile = buildConfig[k].inputFile[f];
                  break;
                }
              }
            } else {
              inputFile = buildConfig[k].inputFile;
            }
          } else {
            inputFile = "index.html";
          }

          switch (buildConfig[k].name) {
            case "RtvEuroAgd":
              inputFile = "RtvEuroAgd.html";
              break;
            case "MediaExpert":
              inputFile = "MediaExpert.html";
              break;
            case "MediaMarkt":
              inputFile = "MediaMarkt.html";
              break;
            case "NeoNet":
              inputFile = "NeoNet.html";
              break;
            default:
              break;
          }

          if (buildConfig[k].flatten) {
            flatFiles[releaseDirName] = releaseDirName + "/index.html";
          }

          if (buildConfig[k].name === "RtvEuroAgd") {
            dom[k + dir] = {
              options: {
                update: {
                  selector: "#video",
                  attribute: "src",
                  value: check_video
                },
                callback: function($, file) {
                  let classExist = $("img").hasClass("lazy");
                  let attribute = $("img").attr("data-original");

                  if (!classExist) {
                    $("img").addClass("lazy");
                  }
                  if (!attribute) {
                    $("img").each(function() {
                      $(this)
                        .attr("data-original", $(this).attr("src"))
                        .attr("src", "f00.esfr.pl/images/www/blank.gif");
                    });
                  }
                }
              },
              src: inputDirName + inputFile,
              dest: inputDirName + inputFile
            };
          } else {
            //TODO: jakoś inaczej spróbować ogarnąć Video
            // dom[k + dir] = {
            //   options: {
            //     update: {
            //       selector: "#video",
            //       attribute: "src",
            //       value: check_video
            //     }
            //   },
            //   src: inputDirName + inputFile,
            //   dest: inputDirName + inputFile
            // };
          }

          for (var a in availablePartials) {
            var repFile =
              partsVariantions +
              availablePartials[a] +
              "/" +
              dir +
              "/" +
              buildConfig[k].name +
              ".html";
            if (!fs.existsSync(repFile)) {
              repFile =
                partsVariantions +
                availablePartials[a] +
                "/" +
                dir +
                "/base.html";
            }

            replacePatterns[k + dir + a] = {
              options: {
                patterns: [
                  {
                    match: "include_" + availablePartials[a],
                    replacement: '<%= grunt.file.read("' + repFile + '") %>'
                  }
                ]
              },
              files: [
                {
                  src: [inputDirName + inputFile],
                  dest: inputDirName + inputFile
                }
              ]
            };
          }

          inlineConfig[k + dir] = {
            options: {
              cssmin: true,
              uglify: true
            },
            src: inputDirName + inputFile,
            dest: releaseDirName + "/index.html"
          };

          copyObj[k + dir] = {
            files: [
              {
                expand: true,
                flatten: buildConfig[k].flatten,
                cwd: inputDirName,
                src: ["assets/*"],
                dest: releaseDirName,
                filter: "isFile"
              }
            ]
          };

          compressObj[k + dir] = {
            options: {
              archive: releaseBase + dir + "_" + buildConfig[k].name + ".zip"
            },
            expand: true,
            cwd: releaseDirName,
            src: ["**/*"],
            dest: "../"
          };

          cleanArr.push(releaseDirName.substr(0, -1) + ".zip");
        }
      }
    }
  } else {
    inlineConfig = {};
    cleanArr = [];
    copyObj = {};
    compressObj = {};
    dom = {};

    for (var k in dirs) {
      dir = dirs[k];
      var tmp = [
        "html_parts/header.html",
        "variations/" + dir + "/" + "index.html",
        "html_parts/footer.html"
      ];
      concatObj[k + dir] = {
        src: tmp,
        dest: "variations/" + dir + "/" + "output.html"
      };

      inlineConfig[dir] = {
        options: {
          cssmin: true,
          uglify: true
        },
        src: "variations/" + dir + "/output.html",
        dest: "../release/preview/" + dir + "/index.html"
      };
      cleanArr.push("../release/preview/" + dir + ".zip");
      copyObj[dir] = {
        files: [
          {
            expand: true,
            cwd: "variations/" + dir + "/",
            src: ["assets/*"],
            dest: "../release/preview/" + dir + "/",
            filter: "isFile"
          }
        ]
      };

      compressObj[dir] = {
        options: {
          archive: "../release/preview/" + dir + ".zip"
        },
        expand: true,
        cwd: "../release/preview/" + dir + "/",
        src: ["**/*"],
        dest: "../"
      };

      dom[dir] = {
        options: {
          update: {
            selector: "#video",
            attribute: "src",
            value: video
          }
        },
        src: "variations/" + dir + "/output.html",
        dest: "variations/" + dir + "/output.html"
      };

      dom[dir] = {
        options: {
          callback: function($, file) {
            let classExist = $("img").hasClass("lazy");
            let attribute = $("img").attr("data-original");

            if (!classExist) {
              $("img").addClass("lazy");
            }
            if (!attribute) {
              $("img").each(function() {
                $(this)
                  .attr("data-original", $(this).attr("src"))
                  .attr("src", "f00.esfr.pl/images/www/blank.gif");
              });
            }
          }
        },
        src: "variations/" + dir + "/index.html",
        dest: "test/" + dir + "/index.html"
      };
    }
  }

  grunt.initConfig({
    concat: concatObj,
    sass: {
      options: {
        implementation: sass,
        sourceMap: true
      },
      dist: {
        files: {
          "css/dev.css": "_scss/dev.scss"
        }
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require("autoprefixer")({ browsers: "last 2 versions" })
          //require('cssnano')()
        ]
      },
      dist: {
        src: "css/dev.css"
      }
    },

    copy: copyObj,

    autoprefixer: {
      dist: {
        files: {
          "css/dev.css": "css/dev.css"
        }
      }
    },

    watch: {
      css: {
        files: ["_scss/**/*.scss"],
        tasks: ["sass", "autoprefixer"]
      },
      html: {
        files: wathcHtmlForChange,
        tasks: ["concat"] //"replace"
      }
    },
    pngmin: pngminObj,
    compress: compressObj,

    clean: {
      build: {
        options: {
          force: true
        },
        src: cleanArr
      }
    },
    inline: inlineConfig,
    "string-replace": {
      flat: {
        files: flatFiles,
        options: {
          replacements: [
            {
              pattern: /assets\//gi,
              replacement: ""
            }
          ]
        }
      }
    },
    replace: replacePatterns,

    dom_munger: dom
  });

  // load common plugins
  grunt.loadNpmTasks("grunt-postcss");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-sass");
  grunt.loadNpmTasks("grunt-contrib-compass");
  grunt.loadNpmTasks("grunt-autoprefixer");
  grunt.loadNpmTasks("grunt-spritesmith");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-inline");
  grunt.loadNpmTasks("grunt-string-replace");
  grunt.loadNpmTasks("grunt-replace");
  grunt.loadNpmTasks("grunt-dom-munger");
  // register tasks

  grunt.registerTask("opt", [], function() {
    grunt.loadNpmTasks("grunt-pngmin");
    grunt.task.run("pngmin");
  });

  grunt.registerTask("remove", [], function() {
    var testFolder = "./variations/";
    var folders = [];
    // console.log("tedt");
    fs.readdirSync(testFolder).forEach(file => {
      folders.push(file);
    });

    for (i in folders) {
      var folder = folders[i];

      if (fs.existsSync(testFolder + folder + "/" + "MediaMarkt.html")) {
        fs.unlinkSync(testFolder + folder + "/" + "MediaMarkt.html");
      }

      if (fs.existsSync(testFolder + folder + "/" + "MediaExpert.html")) {
        fs.unlinkSync(testFolder + folder + "/" + "MediaExpert.html");
      }
      if (fs.existsSync(testFolder + folder + "/" + "RtvEuroAgd.html")) {
        fs.unlinkSync(testFolder + folder + "/" + "RtvEuroAgd.html");
      }
      if (fs.existsSync(testFolder + folder + "/" + "NeoNet.html")) {
        fs.unlinkSync(testFolder + folder + "/" + "NeoNet.html");
      }
      // if (fs.existsSync(testFolder + folder + "/" + "output.html")) {
      //   fs.unlinkSync(testFolder + folder + "/" + "output.html");
      // }
    }
  });

  grunt.registerTask("build", [], function() {
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-compress");
    grunt.loadNpmTasks("grunt-usemin");
    grunt.loadNpmTasks("grunt-processhtml");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-rename");
    grunt.loadNpmTasks("grunt-dom-munger");

    grunt.task.run(
      "sass",
      "concat",
      //"replace",
      "dom_munger",
      "inline",
      "string-replace",
      "clean",
      "copy",
      "compress",
      "remove"
    );
  });
};

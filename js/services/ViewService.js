var ViewService = {
  $el: $('.section.questions .middle'),
  current_view: null,
  render: function(template, data, $to, callback){
    var $target = this.$el


    if(typeof $to != 'undefiend' && $to != null)
      $target = $to

    $target.html('')

    this.current_view = template;
    var jst = (JST['js/templates/'+template+'.swig'])

    if(typeof data == 'undefined' || data == null) {
      $target.append(swig.render( jst() ))
    } else {
      $target.append(swig.render( jst(), { locals: data } ))
    }
 
    if(typeof callback != 'undefined'){
      setTimeout(callback, 200)
    }
  }
}
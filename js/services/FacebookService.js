
var FacebookService = {

	id: null,
	token: null,
	user: null,
	dbu:null,
	init:function(callback){
		var self = this;

		var uid = 'test'
		var token = 'token'

		if(!facebook.user) {
			this.status(function(uid, token, user){
				setTimeout(function(){
				com_interface.get(paths.login, { uid:uid, token: token, user: user }, function(data){
					
					callback(data)

				})
				}, 500)
			})
		}
		/*
		
		//*/

	},
	status: function(callback, dont_login){
		var self = this;
		FB.getLoginStatus(function(response) {
			//alert(response.status)
			if (response.status === 'connected') {
				var uid = response.authResponse.userID;
				var accessToken = response.authResponse.accessToken;
				self.token = accessToken
				self.getMe(uid, accessToken, callback)
				FacebookService.id = uid
			} else if (response.status === 'not_authorized' || response.status === 'unknown') {
				if(typeof dont_login == 'undefined'){
					self.login(callback)
				}
				else
					callback(false)
			} else {
				if(typeof dont_login == 'undefined'){
					self.login(callback)
				}
				else
					callback(false)
			}
		});

	},
	share: function(picture, link, desc ){
		if(typeof picture == 'undefined'){
			picture = facebook.app.picture
		} else if(picture.indexOf('https') !== -1) {
			picture = picture.replace('https', 'http')
		}
		if(typeof link == 'undefined'){
			//link = base_url+'redirtotab'
			link = facebook.app.url
		}
		if(typeof desc == 'undefined'){
			desc = facebook.app.description
		}
		log(link, picture, desc, base_url+'redirtotab')
		FB.ui({
		  method: 'feed',
		  link: link,
		  redirect_url: base_url+'redirtotab',
		  description: desc, // + facebook.app.url,
		  picture: picture
		}, function(response){
			log(response)
		});
	},
	hasPermission: function(name, callback){
		FB.api('/me/permissions', function(r){
			var found = false
			if(r.data && r.data.length){
				for(var k in r.data){
					if(r.data[k].permission == name && r.data[k].status == 'granted'){
						callback(true)
						found = true
					} else if(r.data[k].permission == name && r.data[k].status != 'granted'){
						callback(false)
						found = true
					}
				}
			} else {
				callback(false)
				found = true
			}

			if(!found) {
				callback(false)	
			}
		})
	},
	getApprequests: function(callback){
		if(check_req) {
			
			FB.api('/me/apprequests', function(r){
				//log('apprequests', r)
			   if(r.data && r.data.length){
			   		//log(r.data[0])
					callback(r.data[0])		   	
			   }
			})
		}
	},
	getMe: function(uid, token, callback){
		FB.api('/me', function(response) {
			FacebookService.user = response;
			callback(uid, token, response)
		});
	},
	getFanpageData: function(fpid, callback){
		log('getFanpageData')
		FB.api('/'+fpid, function(response){
			log(response)
			if(response && response.data && response.data.length){
				log(response)

				//callback(response.data)
			} else {
				//callback(false)
			}
		})
	},
	getPhotoAlbums: function(callback){
		FB.api('/me/albums', function(response){
			if(response && response.data && response.data.length){
				log(response)
				callback(response.data)
			} else {
				callback(false)
			}
		})
	},
	setNewAlbum: function(album_name, album_desctiption, callback){
		FB.api('/me/albums', 'post',
		{
			name: album_name,
			message: album_desctiption
		},
		function(response) {
			log('album', response)
			callback(response.id)
		}
		);
	},
	uploadPhotoToAlbum: function(id, url, description, callback){
		log('uploadPhotoToAlbum', id, url, description)
		FB.api('/'+id+'/photos', 'post', {
		    message:description,
		    url:url
		}, function(response){
			log("afterAddPhotoToAlbum", response)
		    if (!response || response.error) {
		        callback(false)
		    } else {
		        callback(response.id)
		    }

		});
	},
	getAlbumPictures: function(album_id, callback){
		FB.api('/'+album_id+'/photos', function(response){
			if(response && response.data && response.data.length){
				callback(response.data)
			} else {
				callback(false)
			}
		})
	},
	getPictureData:function(id, callback){
		FB.api('/'+id, function(response){
			callback(response)
		})
	},

	getProfilePictureURL: function(url){
		return url + "&makeprofile=1&makeuserprofile=1"
	},

	getAllPicturesFromAllAlbums: function(callback){
		FacebookService.getPhotoAlbums(function(albums){
			if(!albums){
				callback(false)
			} else {
				FacebookService.getSingleAlbumPhotos(0,albums,[], function(data){
					if(data && data.length){
						callback(data)
					} else {
						callback(false)
					}
				})
			}
		})
	},
	getAllPictures: function(callback){
		FB.api('/me/photos', function(response){
			if(response && response.data && response.data.length){
				callback(response.data)
			} else {
				callback(false)
			}
		})
	},
	
	getSingleAlbumPhotos: function(i, data, photos, end){
		FacebookService.getAlbumPictures(data[i].id, function(pics){
			var ret = photos.concat(pics)
			if(i == data.length - 1){
				end(ret)
			} else {
				FacebookService.getSingleAlbumPhotos(i+1, data, ret, end)
			}
		})
	},
	getFriends: function(callback){
		FB.api('/me/friends', function(friends) {
			
			FB.api('/me/invitable_friends', function(taggable) {
				log('taggable', taggable)
				var tf = []
				var all = []
				if(taggable && taggable.data && taggable.data.length)
					tf = taggable.data

				if(friends && friends.data && friends.data.length){
					com_interface.get(paths.get_online_friends, { friends: friends.data }, function(res){
						console.log(friends, res.friends)
						if(res.status == 'ok' && res.friends.length){
							all = res.friends.concat(tf)
							callback(all)
						}						
					})
				} else {
					all = tf
					callback(all)
				}

			})
			//callback(uid, token, response)
		});
	},
	invite: function(id, game_id, callback){
		FB.ui({method: 'apprequests',
	      message: 'Zapraszam Cię do rozgrywki!',
	      to: id,
	      data: game_id
	    }, function(response){
	        callback(response)
	    });
	},
	getUserInfo: function(id, callback){
		FB.api('/'+id, function(response){
			callback(response)
		})
	},
	login: function(callback){
		var self = this;
		//alert(FB.login)
		FB.login(function(response) {
			if (response.authResponse) {
				var uid = response.authResponse.userID;
				var accessToken = response.authResponse.accessToken;
				self.token = accessToken
				self.getMe(uid, accessToken, callback)
				FacebookService.id = uid
				// /FacebookService.getApprequests(GameService.checkGame)

			} else {
				//TODO: redirect
				console.log('User cancelled login or did not fully authorize.');
			}
		}, {
			scope: facebook.app.scope, 
			return_scopes: true
		});
	},
	size: function(){
		if(typeof FB != 'undefined' && typeof FB.Canvas != 'undefined'){
	      FB.Canvas.setSize({height: $(document).height()+10});
	    }
	},
	scrollTop: function(){
		if(typeof FB != 'undefined' && typeof FB.Canvas != 'undefined'){
			FB.Canvas.scrollTo(0,0);
		}
	}

}
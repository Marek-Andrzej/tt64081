var CommonFunctions = {
	pad: function(num, size) {
	    var s = num+"";
	    while (s.length < size) s = "0" + s;
	    return s;
	},
	
	getRandomInt: function(min, max) {
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	drawFromArray: function(array){
		var l = array.length;
		var idx = CommonFunctions.getRandomInt(0,l-1);
		return array[idx];
	},
	toMMSS: function (val) {
	    var sec_num = parseInt(val, 10); // don't forget the second param
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (hours   < 10) {hours   = "0"+hours;}
	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}
	    var time    = minutes+':'+seconds;
	    return time;
	},
	isMobile: {
	    Android: function() {
	        return /Android/i.test(navigator.userAgent);
	    },
	    BlackBerry: function() {
	        return /BlackBerry/i.test(navigator.userAgent);
	    },
	    iOS: function() {
	        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
	    },
	    Windows: function() {
	        return /IEMobile/i.test(navigator.userAgent);
	    },
	    any: function() {
	        return true;
	        return (CommonFunctions.isMobile.Android() || CommonFunctions.isMobile.BlackBerry() || CommonFunctions.isMobile.iOS() || CommonFunctions.isMobile.Windows());
	    }
	},
	setCookie: function(cname, cvalue, exdays) {
	    if(typeof exdays == 'undefined')
	    	exdays = 360;

	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	},
	getCookie: function(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	},
	checkCookie: function(cname) {
		return CommonFunctions.getCookie(cname)!="";
	},
	generateUuid: function(){
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
		});
	}, 
	generateUniqueId: function(len){
		if(typeof len == typeof undefined)
			len = 8;
		return ((Math.random()*Math.pow(36,len) << 0).toString(36))
	}
}
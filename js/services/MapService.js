var MapService = {
	map: null,
	elid: 'map',
	center: {
		lat: 52.197974,
		lng: 20.957812
	},
	markers: [],
	options: {
		scrollwheel: false,
		center: null,
		zoom: 8
	},
	init: function(){
		if(!MapService.options.center)
			MapService.options.center = MapService.center

		MapService.map = new google.maps.Map(document.getElementById(MapService.elid), MapService.options);
		return this;
	},
	drawMarker: function(point, click_function){
		if(typeof point == typeof undefined || !point)
			point = MapService.center

		var marker = new google.maps.Marker({
			position: point,
			map: MapService.map,
			title: 'Kliknij aby nawigować'
		});

		MapService.markers.push(marker)

		if(typeof click_function != typeof undefined)
			marker.addListener('click', click_function)

		return this;
	}
}
http_path = "./"
css_dir = "./css"
sass_dir = "./_scss"
images_dir = "./img"
javascripts_dir = "./js"
line_comments = false
output_style = :expanded
relative_assets = true
environment = :development

def get_file_name(filename)
    require 'pathname'
    return Pathname.new(filename).basename.to_s
end

# on_stylesheet_saved do |filename|
#     `ssh -p 2424 krzysiek@localhost '/usr/local/bin/growlnotify -m "Compiled: #{get_file_name(filename)}, invoking LiveReload" && touch /Users/krzysiek/bin/livereload_refresh/livereload.css'`
# end

# on_stylesheet_error do |filename, msg|
#     `ssh -p 2424 krzysiek@localhost '/usr/local/bin/growlnotify -m "ERROR: #{get_file_name(filename)}\n #{msg}"'`
# end
require 'fileutils'

on_stylesheet_saved do |file|
  if file.match('.min') == nil
    require 'compass'

    Compass.add_configuration(
        {
            :css_dir => "css",
            :sass_dir => "_scss",
            :output_style => :compressed
        },
        'min' #ADDING A CONFIG REQUIRES A NAME
    )
    Compass.compiler.compile('_scss/main.scss', 'css/main.min.css')
  end
end